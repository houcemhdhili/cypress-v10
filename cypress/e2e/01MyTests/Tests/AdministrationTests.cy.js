import { homePage } from "../pageObjects/homePage"

describe('My project', ()=>{

    const homepage = new homePage()
    beforeEach(()=>{
        Cypress.on('uncaught:exception', (err, runnable) => {
            // returning false here prevents Cypress from
            // failing the test
            return false
          })

        Cypress.Cookies.preserveOnce('Session')
    
       }) 

    it('Navigate to the App', ()=>{
        homepage.naviguer();
        let nombre = homepage.ApiVerification();
        cy.log(nombre)
    })

    it('sign IN', ()=>{
        homepage.SignIn('john@example.com','123456','John Doe')
    })

    it('search for a book',()=>{
        homepage.Search('JS')

    })

    it('select a book',()=>{
        homepage.selectBook('Node JS Cookbook')
    })

    it('command a book',()=>{
        homepage.BookTheBook()
        cy.wait(300)
        homepage.fillTheAdress('98 rue de javel', 'Paris','75015','France')
    })

    it('verify command information',()=>{
        cy.get('.col > a')

    })
})


