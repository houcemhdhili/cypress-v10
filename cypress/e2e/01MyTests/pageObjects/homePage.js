export class homePage {
    naviguer() {

       cy.visit('http://localhost:3000/');
       cy.wait(1000)
       cy.get('#basic-navbar-nav').should('be.visible')

    }

    ApiVerification(){
        cy.request({
            method: 'GET',
            url: 'http://localhost:3000/api/products?keyword=&pageNumber=1',
        }).then((response)=>{
            expect(response.status).to.eq(200)
            expect(response.body).to.have.property('products')
            const x = response.body.products.length
            cy.log(x)
            
        })
    }

    SignIn(login, pwd, name){
        cy.get('a').contains(' Sign In').click();
        cy.get('#email').type(login)
        cy.get('#password').type(pwd)
        cy.get('button[type="submit"]').contains('Sign In').click()
        cy.get('#username').should((P$)=>{
            expect(P$).to.have.text(name)
        })
    }

    Search(text){
        cy.get('input[placeholder="Search Products..."]').type(text)
        cy.get('form').submit()
        cy.wait(2000)
        cy.get('.card-title').contains(text)
    }

    selectBook(bookname){
        cy.get('.card-title').contains(bookname).click()
        cy.wait(3000)
        cy.get('.btn-block').click()
    }

    BookTheBook(){
        cy.get('button').contains('Proceed To Checkout').click()
    }

    fillTheAdress(adress, city, codePostal, Country){
        cy.get('input[placeholder="Enter address"]').type(adress)
        cy.get('#city').type(city)
        cy.get('#postalCode').type(codePostal)
        cy.get('#country').type(Country)
        cy.get('button').contains('Continue').click()
        cy.wait(3000)
        cy.get('button').contains('Continue').click()
    }

}